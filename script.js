// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Есть определённые символы которые мы не можем просто написать и они будут добавлены в строку. Поэтому нам нужно их экранизировать, а именно добавив / перед символом.

// Які засоби оголошення функцій ви знаєте?
// function declaration, function expression, стрелочная функция

// Що таке hoisting, як він працює для змінних та функцій?
// По простому в JS переменные можно использовать до их объявления. Так же и с функциями


function createNewUser(){
    const firstName = prompt('Enter your Name', 'Maxim');
    const secondName = prompt('Enter your second name', 'Lavrus');
    let birthday = prompt("Enter your birthday date", '29.11.2002')
    let newUser = {
        firstName,
        secondName,
        birthday,
        getLogin(){
            return `${firstName[0].toLowerCase()}${secondName.toLowerCase()}`
        },
        getPassword(){
            let birtdayInform = birthday.split(".")
            return `${firstName[0].toUpperCase()}${secondName.toLowerCase()}${birtdayInform[2]}`
        },
        getAge(){
            let birthdayInform = birthday.split(".")
            let birthdayYear = birthdayInform[2]
            let birthdayMonth = birthdayInform[1] - 1
            let birthdayDay = birthdayInform[0]
            let currentDate = new Date
            let userAge = currentDate.getFullYear() - birthdayYear
            if (userAge <= 0 && birthdayMonth < currentDate.getMonth() && birthdayDay < currentDate.getDate()){
                return 'Вам ещё не исполнился даже годик'
            }
            if (currentDate.getMonth() < birthdayMonth){
                return `Вам полных ${userAge + 1} лет`
            } else {
                return `Вам полных ${userAge} лет`
            }
            if (currentDate.getDate() <= birthdayDay){
                return `Вам полных ${userAge + 1} лет`
            }else {
                return `Вам полных ${userAge} лет`
            }
            return `Вам полных ${userAge} лет`
        }
    }
    return newUser
}
user = createNewUser()

console.log(user)
console.log(user.getPassword())
console.log(user.getAge())

